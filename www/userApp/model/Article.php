<?php 

namespace userApp\model;

class Article
{
	
	private $idArticle,
			$titre,
			$type,
			$genre,
			$label,
			$isDispo,
			$imgLink;

	function __construct()
	{
		
	}

	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
		throw new \Exception($emess);

  	}

  	public function __set($attr_name, $attr_val) {
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	     // throw new \Exception($emess);
	   	}
  	}

  	#####################################################################
	
	public function save() {

	    if (isset($this->idArticle)) {
	      return $this->update();
	    }
	    else {
	    	$r=self::findByTitleOrKeyWord($this->titre);
		    if ($r==false) 
				return $this->insert();
		    else {
				$this->idArticle=$r->idArticle;
				return $this->update();
		    }
	    }	
	}
	

	private function update() {
    
	    if (!isset($this->titre))
	    	return false;

		
	    $pdo = \utils\Connexion::getConnexion();
		
	    $query = $pdo->prepare("UPDATE document SET titre=:titre, type=:type, genre=:genre, label=:label, isDispo=:dispo, imageLink=:link WHERE id=:id");
	    
	    if (isset($this->titre))
	     	$query->bindParam(':titre', $this->titre);
	    else
	    	$query->bindParam(':titre',"null", \PDO::PARAM_STR);

	    $query->bindParam(':id', $this->idArticle, \PDO::PARAM_STR);
	  	$query->bindParam(':type', $this->type, \PDO::PARAM_STR);
	    $query->bindParam(':genre', $this->genre, \PDO::PARAM_STR);
	    $query->bindParam(':label', $this->label, \PDO::PARAM_STR);
	    $query->bindParam(':dispo', $isDispo, \PDO::PARAM_INT);
	    $query->bindParam(':link', $this->imgLink, \PDO::PARAM_INT);
	    	    	  
	    return $query->execute();	
	}


	public function delete() {
    
	    if (!isset($this->idArticle)) {
	    	return 0;
	    }
	      
	    else
	    {
	    	$pdo = \utils\Connexion::getConnexion();
	    	$requete = $pdo->prepare("DELETE FROM document WHERE id = :id;");
	    	$requete->bindParam(':id', $this->idArticle);
	    	$requete->execute();
	    	$count= $requete->rowCount();
	    	return $count;
	    }
	}


	private function insert() {
	    
	    $pdo = \utils\Connexion::getConnexion();

	    $requete = $pdo->prepare("INSERT INTO document ( titre, type, genre, label, isDispo, imageLink ) VALUES ( :titre, :type, :genre, :label, :dispo, :link );");
	  
	    $requete->bindParam(':titre', $this->titre);
	    $requete->bindParam(':type', $this->type);
	    $requete->bindParam(':genre', $this->genre);
	    $requete->bindParam(':label', $this->label);
	    $requete->bindParam(':dispo', $this->isDispo);
	    $requete->bindParam(':link', $this->imgLink);

	    if ($requete->execute()){
	       $this->idArticle = $pdo->LastInsertId();
	       return $this->idArticle;
	    }
	    return -1;
	}

	#####################################################################

	public static function findById($id) {

	    $pdo = \utils\Connexion::getConnexion();
	    
	    $requete = $pdo->prepare("SELECT * FROM document WHERE id=:id;");
	    $requete->bindParam(':id',$id);
	    $resultObject = $requete->execute();
	    if ($resultObject) {
	      while ($ligne = $requete->fetch(\PDO::FETCH_OBJ)) {
	        $new = new Article();
			    $new->idArticle=utf8_decode($ligne->id);
			    $new->titre=utf8_decode($ligne->titre);
			    $new->type=utf8_decode($ligne->type);
			    $new->genre=utf8_decode($ligne->genre);
			    $new->label=utf8_decode($ligne->label);
			    switch ($ligne->isDispo) {
			    	case 2:
			    		$new->isDispo=utf8_decode('Réservé');
			    		break;

			    	case -1:
			    		$new->isDispo='Indisponible';
			    		break;
			    	
			    	case 0:
			    		$new->isDispo=utf8_decode('En prêt');
			    		break;

			    	case 1:
			    		$new->isDispo='Disponible';
			    		break;

			    	case -2:
			    		$new->isDispo=utf8_decode('Réservé');
			    		break;	
			    }
			    $new->imgLink=$ligne->imageLink;
	        return $new;
	      }
	    }
	    else return false;
	}



	public static function findByTitleOrKeyWord($value) {

		$res = array();

		$value = "%".$value."%";
    
	    $pdo = \utils\Connexion::getConnexion();
	    
	    $query = $pdo->prepare("SELECT * FROM document WHERE titre LIKE :keyWord OR label LIKE :keyWordTwo");
	    $query->bindParam(":keyWord", $value);
	    $query->bindParam(":keyWordTwo", $value);
	    $resu = $query->execute();
	
		if (isset($resu)) {
	    while ($ligne = $query->fetch(\PDO::FETCH_OBJ)) {
	      		$new = new Article();
			    $new->idArticle=utf8_decode($ligne->id);
			    $new->titre=utf8_decode($ligne->titre);
			    $new->type=utf8_decode($ligne->type);
			    $new->genre=utf8_decode($ligne->genre);
			    $new->label=utf8_decode($ligne->label);
			    switch ($ligne->isDispo) {
			    	case -1:
			    		$new->isDispo='Indisponible';
			    		break;
			    	
			    	case 0:
			    		$new->isDispo=utf8_decode('En prêt');
			    		break;

			    	case 1:
			    		$new->isDispo='Disponible';
			    		break;

			    	case -2:
			    		$new->isDispo=utf8_decode('Réservé');
			    		break;
			    }
			    $new->imgLink=$ligne->imageLink;
			    $res[] = $new;
	    	}
	    	return $res;
	    }  
	    else return -1;
	}

	public static function findAll() {
		$res = array();
    
	    $pdo = \utils\Connexion::getConnexion();
	    
	    $query = $pdo->prepare("SELECT * FROM document;");
	    $resu = $query->execute();
	
		if (isset($resu)) {
	    while ($ligne = $query->fetch(\PDO::FETCH_OBJ)) {
	      		$new = new Article();
			    $new->idArticle=utf8_decode($ligne->id);
			    $new->titre=utf8_decode($ligne->titre);
			    $new->type=utf8_decode($ligne->type);
			    $new->genre=utf8_decode($ligne->genre);
			    $new->label=utf8_decode($ligne->label);
			    switch ($ligne->isDispo) {
			    	case 2:
			    		$new->isDispo=utf8_decode('Réservé');
			    		break;

			    	case -1:
			    		$new->isDispo='Indisponible';
			    		break;
			    	
			    	case 0:
			    		$new->isDispo=utf8_decode('En prêt');
			    		break;

			    	case 1:
			    		$new->isDispo='Disponible';
			    		break;

			    	case -2:
			    		$new->isDispo=utf8_decode('Réservé');
			    		break;
			    }
			    $new->imgLink=$ligne->imageLink;
			    $res[] = $new;
	    	}
	    	return $res;
	    }  
	    else return 0;
	}

	public static function findByType($type) {

		$res = array();
    
	    $pdo = \utils\Connexion::getConnexion();
	    
	    $query = $pdo->prepare("SELECT * FROM document WHERE type=:type;");
	    $query->bindParam(":type", $type);
	    $resu = $query->execute();
	    
	    if (isset($resu)) {
	    while ($ligne = $query->fetch(\PDO::FETCH_OBJ)) {
	      		$new = new Article();
			    $new->idArticle=utf8_decode($ligne->id);
			    $new->titre=utf8_decode($ligne->titre);
			    $new->type=utf8_decode($ligne->type);
			    $new->genre=utf8_decode($ligne->genre);
			    $new->label=utf8_decode($ligne->label);
			    switch ($ligne->isDispo) {
			    	case -1:
			    		$new->isDispo='Indisponible';
			    		break;
			    	
			    	case 0:
			    		$new->isDispo=utf8_decode('En prêt');
			    		break;

			    	case 1:
			    		$new->isDispo='Disponible';
			    		break;

			    	case -2:
			    		$new->isDispo=utf8_decode('Réservé');
			    		break;
			    }
			    $new->imgLink=$ligne->imageLink;
			    $res[] = $new;
	    	}
	    	return $res;
	    }  
	    else return 0;
	}

	public static function findByGenre($genre) {

		$res = array();
    
	    $pdo = \utils\Connexion::getConnexion();
	    
	    $query = $pdo->prepare("SELECT * FROM document WHERE genre=:genre;");
	    $query->bindParam(":genre", $genre);
	    $resu = $query->execute();
	    
	    if (isset($resu)) {
	    while ($ligne = $query->fetch(\PDO::FETCH_OBJ)) {
	      		$new = new Article();
			    $new->idArticle=utf8_decode($ligne->id);
			    $new->titre=utf8_decode($ligne->titre);
			    $new->type=utf8_decode($ligne->type);
			    $new->genre=utf8_decode($ligne->genre);
			    $new->label=utf8_decode($ligne->label);
			    switch ($ligne->isDispo) {
			    	case -1:
			    		$new->isDispo='Indisponible';
			    		break;
			    	
			    	case 0:
			    		$new->isDispo=utf8_decode('En prêt');
			    		break;

			    	case 1:
			    		$new->isDispo='Disponible';
			    		break;

			    	case -2:
			    		$new->isDispo=utf8_decode('Réservé');
			    		break;
			    }
			    $new->imgLink=$ligne->imageLink;
			    $res[] = $new;
	    	}
	    	return $res;
	    }  
	    else return 0;
	}

	public static function findByDispo($dispo) {

		$res = array();
    
	    $pdo = \utils\Connexion::getConnexion();
	    
	    $query = $pdo->prepare("SELECT * FROM document WHERE isDispo=:dispo;");
	    $query->bindParam(":dispo", $dispo);
	    $resu = $query->execute();
	    
	    if (isset($resu)) {
	    	while ($ligne = $query->fetch(\PDO::FETCH_OBJ)) {
	      		$new = new Article();
			    $new->idArticle=utf8_decode($ligne->id);
			    $new->titre=utf8_decode($ligne->titre);
			    $new->type=utf8_decode($ligne->type);
			    $new->genre=utf8_decode($ligne->genre);
			    $new->label=utf8_decode($ligne->label);
			    switch ($ligne->isDispo) {
			    	case -1:
			    		$new->isDispo='Indisponible';
			    		break;
			    	
			    	case 0:
			    		$new->isDispo=utf8_decode('En prêt');
			    		break;

			    	case 1:
			    		$new->isDispo='Disponible';
			    		break;

			    	case -2:
			    		$new->isDispo=utf8_decode('Réservé');
			    		break;	
			    }
			    $new->imgLink=$ligne->imageLink;
			    $res[] = $new;
	    	}
	    	return $res;
	    }  
	    else return 0;
	}

	public static function listSelectGenre(){
		$res = array();
    
	    $pdo = \utils\Connexion::getConnexion();
	    
	    $query = $pdo->prepare("SELECT genre FROM document ORDER BY genre ASC;");
	    $resu = $query->execute();

	    if (isset($resu)) {
	  	 	while ($ligne = $query->fetch(\PDO::FETCH_OBJ)) {
	  	 		$res[] = utf8_decode($ligne->genre);
	  	 	}
		}

		$res = array_unique($res);	    
		return $res;
	}

	public static function searchAll($titre, $type, $genre, $label, $dispo){
		$res = array();
    
	    $pdo = \utils\Connexion::getConnexion();


	    $sqlwhere = "";

	    if ($titre!="") {
	    	$titre = "%".$titre."%";
	    	$sqlwhere.= " titre LIKE :titre";
	    }
	    elseif ($titre=="") {
	    	$sqlwhere.=" titre IS NOT NULL";
	    }

	    if ($label!="") {
	    	$label = "%".$label."%";
	    	$sqlwhere.=" AND label LIKE :label";
	    }

	    if ($type!="null") {
	    	$sqlwhere.=" AND type=:type";
	    }

	    if ($genre!="null") {
	    	$sqlwhere.=" AND genre = :genre";
	    }

	    if ($dispo!="null") {
	    	$sqlwhere.=" AND isDispo = :dispo";
	    }

	    $query = $pdo->prepare("SELECT * FROM document WHERE ".$sqlwhere);


	    //$query = $pdo->prepare("SELECT * FROM document WHERE isDispo=:dispo;");
	    if ($titre!="") {
	    	$query->bindParam(":titre", $titre);
	    }

	    if ($label!="") {
	    	$query->bindParam(":label", $label);
	    }

	    if ($type!="null") {
	    	$query->bindParam(":type", $type);
	    }

	    if ($genre!="null") {
	    	$query->bindParam(":genre", $genre);
	    }

	    if ($dispo!="null") {
	    	$query->bindParam(":dispo", $dispo);
	    }

	    $resu = $query->execute();
	    
	    if (isset($resu)) {
	    	while ($ligne = $query->fetch(\PDO::FETCH_OBJ)) {
	      		$new = new Article();
			    $new->idArticle=utf8_decode($ligne->id);
			    $new->titre=utf8_decode($ligne->titre);
			    $new->type=utf8_decode($ligne->type);
			    $new->genre=utf8_decode($ligne->genre);
			    $new->label=utf8_decode($ligne->label);
			    switch ($ligne->isDispo) {
			    	case 2:
			    		$new->isDispo=utf8_decode('Réservé');
			    		break;

			    	case -1:
			    		$new->isDispo='Indisponible';
			    		break;
			    	
			    	case 0:
			    		$new->isDispo=utf8_decode('En prêt');
			    		break;

			    	case 1:
			    		$new->isDispo='Disponible';
			    		break;

			    	case 2:
			    		$new->isDispo=utf8_decode('Réservé');
			    		break;
			    }
			    $new->imgLink=$ligne->imageLink;
			    $res[] = $new;
	    	}
	    	return $res;
	    }  
	    else return 0;
	}

	public function reserver() {
		$pdo = \utils\Connexion::getConnexion();
		$pdo2 = \utils\Connexion::getConnexion();

		$requete_preparee = $pdo->prepare("UPDATE document SET isDispo=2 WHERE id=:iddoc");	
		$requete_preparee->bindParam(':iddoc', $this->idArticle);
		$requete_preparee->execute();


		$query = $pdo2->prepare("INSERT INTO reservation (idUser, idDoc, dateReserv) VALUES (:user, :doc, :dateToday)");	
		$query->bindParam(':user', $_SESSION['id']);
		$query->bindParam(':doc', $this->idArticle);
		$date = date("y-m-d");
		$query->bindParam(':dateToday', $date);

		$query->execute();


		return 0;
	}
}