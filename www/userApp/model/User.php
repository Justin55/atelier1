<?php 

namespace userApp\model;

class User
{
	private $idUser, 
			$nom, 
			$prenom, 
			$tel, 
			$mail,
			$dateNaiss,
			$adresse,
			$login,
			$password;

	function __construct()
	{
		
	}


	public function __toString() {
	    return "[". __CLASS__ . "] [ID : ". $this->idUser .
	      "] [Nom : " . $this->nom .
	      "] [Prenom : " . $this->prenom .
	      "] [Telephone : " . $this->tel . 
	      "] [eMail : " . $this->mail .
	      "] [Date de naissance : " . $this->dateNaiss .
	      "] [Adresse : " . $this->adresse .
	      "] [Login : " . $this->login .
	      "] [Password : " . $this->password .
	      "]";
  	}

	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
	    throw new \Exception($emess);
  	}

  	public function __set($attr_name, $attr_val) {
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	      throw new \Exception($emess);
	   	}
  	}

  	#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
  	
  	public function save() {

  		echo $this->idUser;
	    if (isset($this->idUser)) {
	      return $this->update();
	    }
	    else {
	    	$r=self::findById($this->idUser);
		    if ($r==false) 
				return $this->insert();
		    else {
				$this->idUser=$r->idUser;
				return $this->update();
		    }
	    }	
	}

	private function update() {
		
	    $pdo = \utils\Connexion::getConnexion();
		
	    $query = $pdo->prepare("UPDATE user SET nom=:nom, 
	    										prenom=:prenom, 
	    										tel=:tel, 
	    										mail=:mail, 
	    										dateNaiss=:dateN, 
	    										adresse=:adresse, 
	    										login=:login, 
	    										password=:password 
	    						WHERE idUser=:idUser");
	    
	    if (isset($this->idUser))
	     	$query->bindParam(':idUser', $this->idUser);
	     else
	    	$query->bindParam(':idUser',"null", \PDO::PARAM_STR);

	    $query->bindParam(':nom', $this->nom);
	    $query->bindParam(':prenom', $this->prenom);
	    $query->bindParam(':tel', $this->tel);
	    $query->bindParam(':mail', $this->mail);
	    $query->bindParam(':dateN', $this->dateNaiss);
	    $query->bindParam(':adresse', $this->adresse);
	    $query->bindParam(':login', $this->login);
	    $query->bindParam(':password', $this->password);
	    
	    	  
	    return $query->execute();	
	}

	public function delete() {
    
	    if (!isset($this->idUser)) {
	    	return 0;
	    }
	      
	    else
	    {
	    	$pdo = \utils\Connexion::getConnexion();
	    	$requete = $pdo->prepare("DELETE FROM user WHERE idUser = :id;");
	    	$requete->bindParam(':id', $this->idUser);
	    	$requete->execute();
	    	$count= $requete->rowCount();
	    	return $count;
	    }
	}

  	private function insert()
  	{
  		$pdo = \utils\Connexion::getConnexion();

	    $requete = $pdo->prepare("INSERT INTO user ( nom, prenom, tel, mail, dateNaiss, adresse, login, password ) VALUES ( :nom, :prenom, :tel, :mail, :dateNaiss, :adresse, :login, :pass );");
	  
	    $requete->bindParam(':nom', $this->nom);
	    $requete->bindParam(':prenom', $this->prenom);
	    $requete->bindParam(':tel', $this->tel);
	    $requete->bindParam(':mail', $this->mail);
	    $requete->bindParam(':dateNaiss', $this->dateNaiss);
	    $requete->bindParam(':adresse', $this->adresse);
	    $requete->bindParam(':login', $this->login);
	    $requete->bindParam(':pass', $this->password);

	    if ($requete->execute()){
	       $this->idUser = $pdo->LastInsertId();
	       return $this->idUser;
	    }
	    return -1;
  	}

  	#/\#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\

  	public static function findById($id)
  	{

		$pdo = \utils\Connexion::getConnexion();
	    
	    $query = $pdo->prepare("SELECT * FROM user WHERE idUser=:idUser");
	    $query->bindParam(':idUser',$id);
	    $resu = $query->execute();
	    if ($resu) {
	     	while ($ligne = $query->fetch(\PDO::FETCH_OBJ)) {
	       		$new = new User();
	       		$new->idUser = $ligne->idUser;
				$new->nom = $ligne->nom;
				$new->prenom = $ligne->prenom;
				$new->tel = $ligne->tel;
				$new->mail = $ligne->mail;
				$new->dateNaiss = $ligne->dateNaiss;
				$new->adresse = $ligne->adresse;
				$new->login = $ligne->login;
				$new->password = $ligne->password;
	       		return $new;
	   		}
		}
		else return -1;
  	}

  	public static function findByLogin($login)
  	{

		$pdo = \utils\Connexion::getConnexion();
	    
	    $query = $pdo->prepare("SELECT * FROM user WHERE login=:login");
	    $query->bindParam(':login',$login);
	    $resu = $query->execute();
	    if ($resu) {
	     	while ($ligne = $query->fetch(\PDO::FETCH_OBJ)) {
	       		$new = new User();
	       		$new->idUser = $ligne->idUser;
				$new->nom = $ligne->nom;
				$new->prenom = $ligne->prenom;
				$new->tel = $ligne->tel;
				$new->mail = $ligne->mail;
				$new->dateNaiss = $ligne->dateNaiss;
				$new->adresse = $ligne->adresse;
				$new->login = $ligne->login;
				$new->password = $ligne->password;
	       		return $new;
	   		}
	   		return -1;
		}
		else {
			return -1;
  		}
  	}


  	#/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\

  // 	public function connect()
  // 	{
  		
  // 		$pdo = \utils\Connexion::getConnexion();
	    
	 //    $requete = $pdo->prepare("SELECT * FROM user WHERE login=:login");
	 //    $requete->bindParam(':login',$this->login);
	 //    $resu = $requete->execute();
	 //    if ($resu) {
	 //    	$_SESSION['user'] = $this;
	 //    	$serialize = serialize($_SESSION['user']);
	 //    	return $serialize;
		// }
	 //    return false;
  // 	}

  // 	public function disconnect()
  // 	{
  // 		unset($_SESSION['user']);
  // 		$this->login="";
  // 		$this->password="";
  // 		return 0;
  // 	} 	
}
