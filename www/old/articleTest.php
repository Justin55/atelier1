<?php  

require_once("conf/autoload.php");

// Test du champ de recherche "Titre ou mot clé"...

// echo utf8_decode("<h1>Test du champ de recherche 'Titre ou mot clé'...</h1>");

// $test = \userApp\model\Article::findByTitleOrKeyWord("a");

// foreach ($test as $new) {
// 	echo $new->idArticle."<br>";
// 	echo $new->titre."<br>";
// 	echo $new->type."<br>";
// 	echo $new->genre."<br>";
// 	echo $new->label."<br>";
// 	echo $new->isDispo."<br>";
// 	echo $new->imgLink."<br>";
// }

// echo "<hr>";

// // Test du champ de recherche par gencode...

// echo utf8_decode("<h1>Test du champ de recherche par gencode...</h1>");

// $test2 = \userApp\model\Article::findById('3123678349509');
// echo "<b>".$test2->idArticle."</b><br>";
// echo $test2->titre."<br>";
// echo $test2->type."<br>";
// echo $test2->genre."<br>";
// echo $test2->label."<br>";
// echo $test2->isDispo."<br>";
// echo $test2->imgLink."<br>";

// echo "<hr>";

// // Test du champ de recherche par type...

// echo utf8_decode("<h1>Test du champ de recherche par type...</h1>");

// $test3 = \userApp\model\Article::findByType('CD');
// foreach ($test3 as $new) {
// 	echo $new->idArticle."<br>";
// 	echo $new->titre."<br>";
// 	echo "<b>".$new->type."</b><br>";
// 	echo $new->genre."<br>";
// 	echo $new->label."<br>";
// 	echo $new->isDispo."<br>";
// 	echo $new->imgLink."<br>";
// }

// echo "<hr>";

// // Test du champ de recherche par genre...

// echo utf8_decode("<h1>Test du champ de recherche par genre...</h1>");

// $test4 = \userApp\model\Article::findByGenre('Variété');
// foreach ($test4 as $new) {
// 	echo $new->idArticle."<br>";
// 	echo $new->titre."<br>";
// 	echo $new->type."<br>";
// 	echo "<b>".$new->genre."</b><br>";
// 	echo $new->label."<br>";
// 	echo $new->isDispo."<br>";
// 	echo $new->imgLink."<br>";
// }

// echo "<hr>";

// // Test du champ de recherche par disponibilité...
// // -1 : Indisponible
// // 0 : Preté
// // 1 : Disponible

// echo utf8_decode("<h1>Test du champ de recherche par disponibilité...</h1>");

// $test5 = \userApp\model\Article::findByDispo(1);
// foreach ($test5 as $new) {
// 	echo $new->idArticle."<br>";
// 	echo $new->titre."<br>";
// 	echo $new->type."<br>";
// 	echo $new->genre."<br>";
// 	echo $new->label."<br>";
// 	echo "<b>".$new->isDispo."</b><br>";
// 	echo $new->imgLink."<br>";
// }

// echo "<hr>";

// // Test d'insertion dans la BDD'...

// echo utf8_decode("<h1>Test d'insertion dans la BDD...</h1>");

// $article = new \userApp\model\Article();


// $article->titre = "HasanCD2";
// $article->type = "CD";
// $article->genre = "Variété";
// $article->label = "Chansons du film HasanCF2";
// $article->isDispo = 1;
// $article->imgLink = "";

// // $article->save();

// $test = \userApp\model\Article::findByTitleOrKeyWord($article->titre);

// foreach ($test as $new) {
// 	echo $new->idArticle."<br>";
// 	echo $new->titre."<br>";
// 	echo $new->type."<br>";
// 	echo $new->genre."<br>";
// 	echo $new->label."<br>";
// 	echo $new->isDispo."<br>";
// 	echo $new->imgLink."<br>";
// }

// echo "<hr>";

// echo utf8_decode("<h1>Test de modification dans la BDD...</h1>");

// $article->isDispo = 0;
// // $article->save();

// $test2 = \userApp\model\Article::findByTitleOrKeyWord($article->titre);

// foreach ($test2 as $new) {
// 	echo $new->idArticle."<br>";
// 	echo $new->titre."<br>";
// 	echo $new->type."<br>";
// 	echo $new->genre."<br>";
// 	echo $new->label."<br>";
// 	echo $new->isDispo."<br>";
// 	echo $new->imgLink."<br>";
// }

// echo "<hr>";

// echo utf8_decode("<h1>Test de suppression dans la BDD...</h1>");

// $deleteArticle = \userApp\model\Article::findById('9845193109400');

// // $deleteArticle->delete();

// $test3 = \userApp\model\Article::findByTitleOrKeyWord($deleteArticle->titre);


// echo "<hr>";

// echo utf8_decode("<h1>Test d'affichage des genres depuis la BDD...</h1>");

// $genre = \userApp\model\Article::listSelectGenre();

// echo utf8_decode("<select name='genre'>
// 		<optgroup label='-- Sélectionner le genre --'>");
// foreach ($genre as $val) {
// 	echo "<option name='".strtolower($val)."'>".$val."</option>";
// }
// echo "</select>";
// echo "<hr>";
 

echo utf8_decode("<h1>Test du champ de recherche multi critères...</h1>");

$http = new \utils\HttpRequest();
$testss = \userApp\model\Article::searchAll();
$genre = \userApp\model\Article::listSelectGenre();

$form = "<div>
					<form action=\"/".$http->racine."/articleTest.php\" method=\"POST\">
						<input type=\"text\" name=\"titre\" placeholder=\"Rechercher par titre ou mots clé...\" class=\"search\">
			
					<select name=\"type\" class=\"search\">
						<optgroup label='-- Sélectionner le support --'>
						<option value=\"livre\">Livre</option> 
						<option value=\"cd\">CD</option>
						<option value=\"dvd\">DVD</option>
					</select>
					
					<select name='genre' class=\"search\">
						<optgroup label='-- Sélectionner le genre --'>";
					foreach ($genre as $val) {
						$form.="<option name='".utf8_encode(strtolower($val))."'>".utf8_encode($val)."</option>";
					}

					$form.="</select>
				
					<select name=\"dispo\" class=\"search\">
						<optgroup label='-- Disponibilité --'>
						<option value=\"null\">Aucun Status</option>
						<option value=\"-1\">Indisponible</option> 
						<option value=\"0\">Prêté</option>
						<option value=\"1\">Disponible</option>
					</select>
					<input type=\"submit\" name=\"rechercher\" value=\"Rechercher...\" class=\"search btn btnGreen\">
				</form>
			</div>";
			echo $form;



// foreach ($testss as $new) {
// 	echo $new->idArticle."<br>";
// 	echo $new->titre."<br>";
// 	echo $new->type."<br>";
// 	echo $new->genre."<br>";
// 	echo $new->label."<br>";
// 	echo $new->isDispo."<br>";
// 	echo $new->imgLink."<br>";
// }

echo "<hr>";
