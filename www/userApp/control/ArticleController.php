<?php 

namespace userApp\control;

/**
* 
*/
class ArticleController
{
	private $http;
	
	function __construct($httpObject)
	{
		$this->http = $httpObject;
	}

	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
	    throw new \Exception($emess);
  	}

  	public function __set($attr_name, $attr_val) 
	{
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	      throw new \Exception($emess);
	    }
	}

	public function dispatch()
	{
		switch ($this->http->action) {
			case 'search':
				return $this->search();
			break;

			case 'searchID':
				return $this->idSearch();
			break;

			case 'searchType':
				return $this->typeSearch();
			break;

			case 'searchGenre':
				return $this->genreSearch();
			break;

			case 'searchDispo':
				return $this->dispoSearch();
			break;

			case 'reserve':
				return $this->reserve();
			break;

			case 'log':
				return $this->userHeader();
			break;

			case 'deco':
				return $this->disconnect();
			break;
		}
	}

	// private function articleSearch()
	// {
	// 	if (isset($_POST['titreKeyWord'])) {
	// 		$value = $_POST['titreKeyWord'];
	// 		$new = \userApp\model\Article::findByTitleOrKeyWord($value);
	// 		$view = new \userApp\vue\View();
	// 		$view->research = $new;
	// 		echo $view->afficher('search');
	// 	}

	// 	elseif ($this->http->param=="all-article") {
	// 		$new = \userApp\model\Article::findAll();
	// 		$view = new \userApp\vue\View();
	// 		$view->research = $new;
	// 		echo $view->afficher('all-article');
	// 	}

	// 	else {
	// 		// $new = \userApp\model\Article::findByTitleOrKeyWord($this->http->param);
	// 		$view = new \userApp\vue\View();
	// 		// $view->research = $new;
	// 		echo $view->afficher('searchNew');
	// 	}
	// }

	private function idSearch()
	{
		$new = \userApp\model\Article::findByID($this->http->param);
		$view = new \userApp\vue\View();
		$view->research = $new;
		echo $view->afficher('id');
	}

	private function typeSearch()
	{
		$value = $_POST['type'];
		$new = \userApp\model\Article::findByType($value);
		$view = new \userApp\vue\View();
		$view->research = $new;
		echo $view->afficher('type');
	}

	private function genreSearch()
	{
		$value = $_POST['genre'];
		$new = \userApp\model\Article::findByGenre($value);
		$view = new \userApp\vue\View();
		$view->research = $new;
		echo $view->afficher('genre');
	}

	private function dispoSearch()
	{
		$value = $_POST['dispo'];
		$new = \userApp\model\Article::findByDispo($value);
		$view = new \userApp\vue\View();
		$view->research = $new;
		echo $view->afficher('dispo');
	}

	private function userHeader(){
		if (isset($_POST['login'])) {
			$login = $_POST['login'];
			$pass = $_POST['passw'];	

			$test = \userApp\model\User::findByLogin($login);
			if (!is_int($test)) {
				if (password_verify($pass, $test->password)) {
				$_SESSION['user'] = $test->login;
				$_SESSION['id'] = $test->idUser;
				$view = new \userApp\vue\View();
				echo $view->afficher('index');
				}
			}
			else
			{
				$view = new \userApp\vue\View();
				echo $view->afficher('index');
			}
		}
	}

	private function disconnect(){
		unset($_SESSION['user']);
		$view = new \userApp\vue\View();
		echo $view->afficher('index');
	}

	private function search()
	{
		if (isset($_POST['titre'])) {
			$type = $_POST["type"];
		    $genre = $_POST["genre"];
		    $titre = $_POST["titre"];
		    $label = $_POST["keyword"];
		    $dispo = $_POST["dispo"];
		    $new = \userApp\model\Article::searchAll($titre, $type, $genre, $label, $dispo);
		    $view = new \userApp\vue\View();
		    $view->research = $new;
			echo $view->afficher('search');
		}

		elseif ($this->http->param=="all-article") {
			$new = \userApp\model\Article::findAll();
			$view = new \userApp\vue\View();
			$view->research = $new;
			echo $view->afficher('all-article');
		}

		else {
			// $new = \userApp\model\Article::findByTitleOrKeyWord($this->http->param);
			$view = new \userApp\vue\View();
			// $view->research = $new;
			echo $view->afficher('searchNew');
		}	
	}

	private function reserve()
	{
		$new = \userApp\model\Article::findById($_POST['reserve']);
		$new->reserver();
		$new = \userApp\model\Article::findById($_POST['reserve']);
		$view = new \userApp\vue\View();
		$view->research = $new;
		echo $view->afficher('id');
	}

}