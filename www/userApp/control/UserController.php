<?php  

namespace userApp\control;

class UserController{

	private $http;
	
	function __construct($httpObject)
	{
		$this->http = $httpObject;
	}

	public function dispatch()
	{
		switch ($this->http->action){
			case 'head':
				return $this->userHeader();
				break;

		}
	}

	private function userHeader(){
		if (isset($_POST['login'])) {
			$login = $_POST['login'];
			$pass = $_POST['passw'];	

			$test = \userApp\model\User::findByLogin($login);
			if (password_verify($pass, $test->password)) {
			    if ($test->connect()!=false) {
				    $temp = unserialize($test->connect());
					$view = new \userApp\vue\UserView();
					$view->user = $temp;
					echo $view->afficher('login');
				}
			} 
			else {
				$view = new \userApp\vue\UserView();
			    echo $view->afficher('connectPlus');  
			} 
		}
		else
		{
			$view = new \userApp\vue\UserView();
			echo $view->afficher('connect');
		}
	}
}