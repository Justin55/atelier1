<?php

/***
*
* author : Henner Frédéric et Merlier Florian
*
***/

namespace staffApp\controller;

class StaffController
{

	private $http;


	function __construct($object)
	{
		$this->http = $object;
	}

	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
	    //throw new \Exception($emess);
  	}

  	public function __set($attr_name, $attr_val) 
	{
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	      //throw new \Exception($emess);
	    }
	}

	###########################################################################################################
	function dispatch()
	{
		$action = $this->http->action ;
		$param = $this->http->param ;

		switch ($action) 
		{
			case 'emprunt':				// Affichage page emprunt
				$this->affichageEmprunt();
				break;
			case 'retour':				// Affichage page retour 
				$this->affichageRetour();
				break;
			case 'recapitulatif_all':	// Affichage de tout les emprunts encore en cours
				$this->affichageRecapitulatifEmprunt();
				break;	
			case 'recapitulatif_retour':	// Affichage des récapitulatif par rapport à un id
				$this->affichageRecapitulatifRetour();
				break;			
			case 'retourRecapitulatif':		// Affichage du récapitulatif après plusieurs retours de DOC
				$this->affichageRecapitulatifApresRetour();
				break;
			case 'empruntRecapitulatif':	// Affichage du récapitulatif après plusieurs emprunts de DOC
				$this->affichageRecapitulatifApresEmprunt();
				break;
			case 'document':				// Affichage de la page Gérer Document
				$this->affichageDocument();
				break;
			case 'adherent':				// Affichage de la page Gérer Adherents
				$this->affichageAdherent();
				break;
			case 'emprunt_add':				// Redirection vers le script qui ajoute un emprunt 
				$this->ajoutEmprunt();
				break;
			case 'retour_add':				// Redirection vers le script qui retire un emprunt = ajoute un retour
				$this->ajoutRetour();
				break;
			case 'document_add':			// Redirection vers le script qui ajoute un document
				$this->ajoutDocument();
				break;
			case 'adherent_add':			// Redirection vers le script qui ajoute un adherent
				$this->ajoutAdherent();
				break;
			case 'adherent_mod':			// Redirection vers le script qui envoie vers la page de modif des infos de l'adhérent ou le supprime de la BD
				$this->updateAdherent();
				break;
			case 'adherent_updateTest':		// Redirection vers le script qui valide les modifications des adhérents
				$this->updateTest();
				break;
			case 'document_mod':
				$this->updateDocument();
				break;
			case 'document_mod_script':
				$this->modifDocument();
		}
	}	
// Renvoie vers la page emprunt qui permet d'ajoute un emprunt / ou d'aller vers differents récapitulatif
	public function affichageEmprunt()
	{
		$e = new \staffApp\model\Emprunt();
		$v = new \staffApp\view\View();
		echo $v->afficher('emprunt');
	}
// Renvoie vers la page retour qui permet d'ajoute un retour / ou d'aller vers differents récapitulatif
	public function affichageRetour()
	{
		$e = new \staffApp\model\Emprunt();
		$v = new \staffApp\view\View();	
		echo $v->afficher('retour');		
	}
// Renvoie vers la page récapitulatif qui affiche tout les emprunts encore en cours
	public function affichageRecapitulatifEmprunt()
	{
		$e = new \staffApp\model\Emprunt();

		if (isset($_POST['iduser2'])) {
			$e->idUser = $_POST['iduser2'];
			
			$v = new \staffApp\view\View();
			$v->temp = $e->idUser;
			echo $v->afficher('recapitulatif_id');
		}
		else 			{
			$v = new \staffApp\view\View();
			echo $v->afficher('recapitulatif_all');		
		}
	}
// Renvoie vers la page récapitulatif qui affiche tout les emprunts encore en cours par rapport à un id d'adhérent
	public function affichageRecapitulatifRetour()
	{
		$e = new \staffApp\model\Emprunt();

		$e->idUser = $_POST['iduser2'];
			
		$v = new \staffApp\view\View();
		$v->temp = $e->idUser;
		echo $v->afficher('recapitulatif_id');
	}
// Renvoie vers la page emprunt qui affichage le récapitulatif après plusieurs retours de DOC
	public function affichageRecapitulatifApresRetour()
	{
		$e = new \staffApp\model\Emprunt();

		$v = new \staffApp\view\View();
		echo $v->afficher('recapitulatif_retour');		
	}
// Renvoie vers la page emprunt qui affichage le récapitulatif après plusieurs emprunts de DOC
	public function affichageRecapitulatifApresEmprunt()
	{
		$e = new \staffApp\model\Emprunt();

		$v = new \staffApp\view\View();
		echo $v->afficher('recapitulatif_emprunt');		
	}
// Renvoie vers la page document qui permet d'ajouter un document / de les lister / modifier / supprimer
	public function affichageDocument()
	{
		$e = new \staffApp\model\Emprunt();
		$v = new \staffApp\view\View();
		echo $v->afficher('document');
	}
// Renvoie vers la page document qui permet d'ajouter un adhérent / de les lister / modifier / supprimer
	public function affichageAdherent()
	{
		$e = new \staffApp\model\Emprunt();
		$v = new \staffApp\view\View();
		echo $v->afficher('adherent');
	}
// Script qui enregistre un emprunt dans la table emprunt et modifier la valeur de isDispo dans la table document
	public function ajoutEmprunt()
	{
		$temp_id= ""; $temp_doc= ""; $temp= ""; $tabl = array();
		$e = new \staffApp\model\Emprunt();

		$ID = $e->findAllIDUser();
		$DOC = $e->findAllDocDocument();

		$e->idUser = $_POST['iduser'];
		$e->idDoc = $_POST['iddoc'];
		$e->dateEmprunt = date("y-m-d");

		foreach ($ID as $value) {
			if ($value->idUser==$e->idUser) {
				$temp_id="true";
			}
		}

		foreach ($DOC as $value) {
			if ( ($value->id==$e->idDoc)&&($value->isDispo=="1") )  {
				$temp_doc="true";
				$temp="Disponible";
			}
		}

		if ( ($temp_id=="true")&&($temp_doc=="true")&&($temp=="Disponible") ) {
			if (!isset($_SESSION['ID'])) {
				$_SESSION['ID']='';
				$_SESSION['ID']=$_POST['iduser'];
			}
			else
				$_SESSION['ID']=$_POST['iduser'];				

			if (isset($_SESSION['tableauEmprunt'])) {
				$tabl = $_SESSION['tableauEmprunt'];
				$tabl[] = $_POST['iddoc'];
				$_SESSION['tableauEmprunt']=$tabl;
			}
			else 			{
				$_SESSION['tableauEmprunt']='';
				$tabl = $_SESSION['tableauEmprunt'];
				$tabl[] = $_POST['iddoc'];
				$_SESSION['tableauEmprunt']=$tabl;
			}
			$e->insertionEmprunt();

			$v = new \staffApp\view\View();
			echo $v->afficher('emprunt');
		}
		elseif ($temp!="Disponible") {
			$v = new \staffApp\view\View();
			echo $v->afficher('emprunt');
			echo "<p>Le document n'existe pas ou n'est pas disponible.</p>";			
		}
		else 			{
			$v = new \staffApp\view\View();
			echo $v->afficher('emprunt');
			echo "<p>L'adhérent ou le document n'est pas reconnu.</p>";
		}
	}
// Script qui enregistre un retour dans la table emprunt et modifier la valeur de isDispo dans la table document
	public function ajoutRetour()
	{
		$temp= ""; $temp_dispo= ""; $tabl = array();
		$r = new \staffApp\model\Retour();

		$DOCemp = $r->findAllDocEmprunt();
		$DOCdoc = $r->findAllDocDocument();
		$IDuserEmp = $r->findIDbyIDdoc($_POST['iddoc']);

		$r->idDoc = $_POST['iddoc'];

		foreach ($DOCemp as $value) {
			if ($value->idDoc==$r->idDoc) {
				$temp="true";
			}
		}

		foreach ($DOCdoc as $value) {
			if ( ($value->id==$r->idDoc)&&($value->isDispo=="1") ) {
				$temp_dispo="true";
			}
		}

		if ($temp=="true") {

			if (!isset($_SESSION['IDr'])) {
				$_SESSION['IDr']='';
				$_SESSION['IDr']=$IDuserEmp;
			}
			else
				$_SESSION['ID']=$IDuserEmp;				

			if (isset($_SESSION['tableauRetour'])) {
				$tabl = $_SESSION['tableauRetour'];
				$tabl[] = $_POST['iddoc'];
				$_SESSION['tableauRetour']=$tabl;
			}
			else 			{
				$_SESSION['tableauRetour']='';
				$tabl = $_SESSION['tableauRetour'];
				$tabl[] = $_POST['iddoc'];
				$_SESSION['tableauRetour']=$tabl;
			}

			$r->insertionRetour();
	
			$v = new \staffApp\view\View();
			echo $v->afficher('retour');
		}
		else 				{
			if ($temp_dispo=="true") {
				$v = new \staffApp\view\View();
				echo $v->afficher('retour');
				echo "<p>Le document est dans le catalogue.</p>";
			}
			else 			{
				$v = new \staffApp\view\View();
				echo $v->afficher('retour');
				echo "<p>La réference du document n'est pas exacte</p>";				
			}
		}
	}
// Script qui permet d'ajouter un document à la table document
	public function ajoutDocument()
	{
		$a = new \userApp\model\Article();

		$a->titre = $_POST['nom'];
		$a->type = $_POST['type'];
		$a->genre = $_POST['genre'];
		$a->label = $_POST['description'];
		$a->isDispo = 1;

		$extensionOk = array('jpg', 'bmp', 'png', 'gif', 'tiff');

		$name = $_FILES['icone']['name'];
		$info = pathinfo($name);
		$extension = $info['extension'];

		if(in_array($extension, $extensionOk))
		{
			move_uploaded_file($_FILES['icone']['tmp_name'], 'img/produits/'.basename($_FILES['icone']['name']));
				$a->imgLink = $name;
				$a->save();

				$v = new \staffApp\view\View();
				echo $v->afficher('document');	
		}
		else
		{
			$message = "Impossible de stocker le fichier, vérifier l'extension...";
		}	
	}
// Script qui permet d'ajouter un adhérent à la table user
	public function ajoutAdherent()
	{
		$temp_tel= "";
		$temp_mail="";
		$temp_date="";
		$a = new \staffApp\model\Adherent();

		$a->nom = $_POST['nom'];
		$a->prenom = $_POST['prenom'];
		$a->adresse = $_POST['adresse'];
		$a->dateNaissance = $_POST['age'];
		$a->mail = $_POST['mail'];
		$a->tel = $_POST['telephone'];
		$a->login = $_POST['login'];
		$a->password = $_POST['password'];

		$motif ='`^0[6-7][0-9]{8}$`';
		if(preg_match($motif,$_POST['telephone']))
		{
			$temp_tel="true";
		}

		if(filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL))
		{
    		$temp_mail="true";
		}

		if (preg_match("^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$^",$_POST['age']))
   		{
        	$temp_date="true";
    	}
    	

		if ($temp_tel=="true"&&($temp_mail=="true")&&($temp_date=="true")) {
			$a->insertionAdherent();

			$v = new \staffApp\view\View();
			echo $v->afficher('adherent');
		}
		elseif ($temp_tel=="true"&&($temp_mail!="true")&&($temp_date=="true"))		
		{
			$v = new \staffApp\view\View();
			echo $v->afficher('adherent');
			echo "<p class='center'>L'adresse mail saisie n'est pas valide. </p>";			
		}
	
		elseif ($temp_tel!="true"&&($temp_mail=="true")&&($temp_date=="true"))
		{
			$v = new \staffApp\view\View();
			echo $v->afficher('adherent');
			echo "<p class='center'>Le numéro de téléphone n'est pas valide. </p>";
		}
		elseif ($temp_tel=="true"&&($temp_mail=="true")&&($temp_date!="true"))
		{
			$v = new \staffApp\view\View();
			echo $v->afficher('adherent');
			echo "<p class='center'>La date de naissance n'est pas valide. </p>";
		}

		elseif ($temp_tel!="true"&&($temp_mail!="true")&&($temp_date=="true"))
		{
			$v = new \staffApp\view\View();
			echo $v->afficher('adherent');
			echo "<p class='center'>Le numéro de téléphone n'est pas valide. </p>
				  <p class='center'>L'adresse mail saisie n'est pas valide.</p>";
		}

		elseif ($temp_tel!="true"&&($temp_mail=="true")&&($temp_date!="true"))
		{
			$v = new \staffApp\view\View();
			echo $v->afficher('adherent');
			echo "<p class='center'>Le numéro de téléphone n'est pas valide. </p>
				  <p class='center'>La date de naissance n'est pas valide.</p>";
		}

		elseif ($temp_tel=="true"&&($temp_mail!="true")&&($temp_date!="true"))
		{
			$v = new \staffApp\view\View();
			echo $v->afficher('adherent');
			echo "<p class='center'>L'adresse mail saisie n'est pas valide. </p>
				  <p class='center'>La date de naissance n'est pas valide.</p>";
		}

		elseif ($temp_tel!="true"&&($temp_mail!="true")&&($temp_date!="true"))
		{
			$v = new \staffApp\view\View();
			echo $v->afficher('adherent');
			echo "<p class='center'>Le numéro de téléphone n'est pas valide. </p>
				  <p class='center'>L'adresse mail saisie n'est pas valide. </p>
				  <p class='center'>La date de naissance n'est pas valide.</p>";
		}
	}
// Renvoie vers la page de modif des infos de l'adhérent ou le supprime de la BD
	public function updateAdherent()
	{
		if($_POST['name_action']=="Modifier")
		{
			$v = new \staffApp\view\View();
			$v->temp = $_POST['idUser'];
			echo $v->afficher('adherent_mod');
		}

		
		elseif($_POST['name_action']=="Supprimer")
		{
			$a = new \staffApp\model\Adherent();
			$id = $_POST['idUser'];

			$a->deleteAdherent($id);

			$v = new \staffApp\view\View();
			echo $v->afficher('adherent');
		} 
	}
// Modification d'un adhérent dans la BD.
		public function updateTest()
		{
			$u = \userApp\model\User::findById($_POST['id']);
			$u->nom = $_POST['nom'];
			$u->prenom = $_POST['prenom'];
			$u->adresse = $_POST['adresse'];
			$u->dateNaiss = $_POST['age'];
			$u->mail = $_POST['mail'];
			$u->tel = $_POST['telephone'];

			$u->save();
			$v = new \staffApp\view\View();
			$v->temp = $_POST['id'];
			
			echo $v->afficher('adherent');
		}

	public function updateDocument()
	{
		if($_POST['name_action']=="Modifier")
		{
			$v = new \staffApp\view\View();
			$v->temp = $_POST['idDoc'];
			echo $v->afficher('document_mod_script');
		}

		
		elseif($_POST['name_action']=="Supprimer")
		{
			$id = $_POST['idDoc'];
			$d = \userApp\model\Article::findById($id);
			$d->delete();

			$v = new \staffApp\view\View();
			echo $v->afficher('document');
		} 
	}	

	public function modifDocument()
		{
			$a = \userApp\model\Article::findById($_POST['idDoc']);
			$a->titre = $_POST['titre'];
			$a->type = $_POST['type'];
			$a->genre = $_POST['genre'];
			$a->label = $_POST['label'];

			$a->save();
			$v = new \staffApp\view\View();
			$v->temp = $_POST['idDoc'];
			
			echo $v->afficher('document');
		}
}

