<?php 

namespace userApp\vue;

/**
* 
*/
class View
{

	private $research, $user; 


	public function __get($attr_name) 
	{
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
	    throw new \Exception($emess);
	}

	public function __set($attr_name, $attr_val) 
	{
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	      throw new \Exception($emess);
	    }
	}


	function afficher($action)
	{
		$html = "";

		$http = new \utils\HttpRequest();
		$head = $this->afficherHead();
		$menu = $this->afficherMenu();
		$foot = $this->afficherFoot();

		$html = "<!DOCTYPE html>
				<html lang=\"fr\">
				<head>
					<meta charset=\"UTF-8\">
					<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
					<title>MédiaNet</title>
					<link rel=\"icon\" type=\"image/png\" href=\"/".$http->racine."/img/favicon/favicon.png\" />
					<link href=\"/".$http->racine."/style/screen.css\" media=\"screen\" rel=\"stylesheet\" type=\"text/css\" />
					<link href=\"/".$http->racine."/style/screen.css\" media=\"print\" rel=\"stylesheet\" type=\"text/css\" />
					<!--[if IE]>
					   	<link href=\"../".$http->racine."/style/screen.ie.css\" media=\"screen\" rel=\"stylesheet\" type=\"text/css\" />
					<![endif]-->
				</head>
				<body>";

		$html.=$head;
		$html.=$menu."<div>";

		switch ($action) {
				case 'search':
					$html.=$this->afficherRecherche();
				break;

				case 'all-article':
					$html.=$this->afficherCatalogue();
					break;

				case 'searchNew':
					$html.=$this->afficherForm();
				break;

				case 'id':
					$html.=$this->afficherID();
				break;

				case 'index':
					$html.=$this->afficherIndex();
				break;
				}
		$html.="</div>".$foot;
		$html.="</body></html>";			

		return $html;
	}

	function afficherRecherche()
	{
		$http = new \utils\HttpRequest();
		$pdo = \utils\Connexion::getConnexion();

		$form = $this->afficherForm();
		$search = "";
		$search.=$form;
		foreach ($this->research as $new) {
			$search.="<article class=\"jumboV2\">";
			$search.="<h4 class='titleHeight'>".utf8_encode($new->titre)."</h4>";
			$search.="<img src='/".$http->racine."/img/produits/".$new->imgLink."' alt='Image de la ressource ".utf8_encode($new->titre)."'>";
			$search.="<p class='underline'>Media : </p><p>".utf8_encode($new->type)."</p>";
			$search.="<p class='underline'>Catégorie : </p><p>".utf8_encode($new->genre)."</p>";
			$search.="<p class='underline'>Description : </p><p class='label'>".utf8_encode($new->label)."</p>";
			$search.="<p class='underline'>Référence : </p><p>".$new->idArticle."</p>";
			switch ($new->isDispo) {
				case utf8_decode('Réservé'):
					$search.="<p class='alertInfo'>".utf8_encode($new->isDispo)."</p>";
					break;

				case 'Disponible':
					$search.="<p class='alertSucces'>".utf8_encode($new->isDispo)."</p>";
					break;
				
				case 'Indisponible':
					$search.="<p class='alertDanger'>".utf8_encode($new->isDispo)."</p>";
					break;

				case utf8_decode('En prêt'):
					$id = $new->idArticle;
					$requete2 = $pdo->prepare("SELECT dateEmprunt FROM emprunt WHERE idDoc=:id;");
				    $requete2->bindParam(':id', $id);
				    $resultObject2 = $requete2->execute();
				    if ($resultObject2) {
		    			while ($ligne2 = $requete2->fetch(\PDO::FETCH_OBJ)) {
		    				$search.="<p class='alertWarning'>".utf8_encode($new->isDispo)." - Retour le ".date("d-m-y", strtotime($ligne2->dateEmprunt.'+ 30days'))."</p>";
		    			}
		    		}
					break;


				case utf8_decode('Réservé'):
					$search.="<p class='alertInfo'>".utf8_encode($new->isDispo)."</p>";
					break;	
			}
			// $search.="<p>".utf8_encode($new->isDispo)."</p>";
			$search.="<a href='/".$http->racine."/index.php/ArticleController/searchID/".$new->idArticle."'>Voir plus...</a>";
			$search.="</article>";
		}
		return $search;
	}

	function afficherID()
	{
		$http = new \utils\HttpRequest();
		$pdo = \utils\Connexion::getConnexion();

		$html = "";
		$html.="<article class='jumboBig'>";
		$html.="<h2 class='center'>".utf8_encode($this->research->titre)."</h2>";
		$html.="<img src='/".$http->racine."/img/produits/".$this->research->imgLink."' alt='Image de la ressource ".utf8_encode($this->research->titre)."'>";
		$html.="<p class='underline'>Media : </p><p>".utf8_encode($this->research->type)."</p>";
		$html.="<p class='underline'>Catégorie : </p><p>".utf8_encode($this->research->genre)."</p>";
		$html.="<p class='underline'>Description : </p><p>".utf8_encode($this->research->label)."</p>";
		$html.="<p class='underline'>Référence : </p><p>".$this->research->idArticle."</p>";
		switch ($this->research->isDispo) {
			case utf8_decode('Réservé'):
				$html.="<p class='alertInfo'>".utf8_encode($this->research->isDispo)."</p>";
				break;

			case 'Disponible':
				$html.="<p class='alertSucces'>".utf8_encode($this->research->isDispo)."</p>";
				if (isset($_SESSION['user'])) {
					$html.="<form method='POST' action='/".$http->racine."/index.php/ArticleController/reserve'>
								<input type='hidden' name='reserve' value='".$this->research->idArticle."'>
								<input type='submit' name='reserv' value='Reserver' class='btn search btnOrange'>
							</form>";
				}
				break;
			
			case 'Indisponible':
				$html.="<p class='alertDanger'>".utf8_encode($this->research->isDispo)."</p>";
				break;

			case utf8_decode('En prêt'):
				$id = $this->research->idArticle;
				$requete2 = $pdo->prepare("SELECT dateEmprunt FROM emprunt WHERE idDoc=:id;");
				$requete2->bindParam(':id', $id);
				$resultObject2 = $requete2->execute();
				if ($resultObject2) {
		    		while ($ligne2 = $requete2->fetch(\PDO::FETCH_OBJ)) {
		    			$html.="<p class='alertWarning'>".utf8_encode($this->research->isDispo)." - Retour le ".date("d-m-y", strtotime($ligne2->dateEmprunt.'+ 30days'))."</p>";
		    		}
		    	}
				break;

			case utf8_decode('Réservé'):
				$html.="<p class='alertInfo'>".utf8_encode($this->research->isDispo)."</p>";
				break;	
		}
			
		$html.="</article>";
		return $html;
	}
	

	function afficherHead()
	{
		$head="<header>
				<div>
					<h1>MédiaNet</h1>
					<h2>By Team Beefy</h2>
				</div>";

				if (isset($_SESSION['user'])) {
					$http = new \utils\HttpRequest();
					$head.="<div>
						<h2>Bienvenue ".$_SESSION['user']."</h2>
						<a href=\"/".$http->racine."/index.php/ArticleController/deco\" name=\"deco\" class=\"btn\">Deconnexion</a>
						</div>";
				}
				else
				{
					$http = new \utils\HttpRequest();
					$head.="<div>
							<form method=\"POST\" action='/".$http->racine."/index.php/ArticleController/log'>
								<input type=\"text\" name=\"login\" placeholder=\"Login...\" >
								<input type=\"password\" name=\"passw\" placeholder=\"Password...\" >
								<input type=\"submit\" name=\"connect\" value=\"Se connecter\" class=\"btn\">
							</form>	
							</div>";
				}
		$head.="</header>";
		return $head;			
	}

	function afficherIndex()
	{
		$http = new \utils\HttpRequest();
		$index="<article class='jumboBig'>
					<h2 class='center'>Bienvenue sur l'application Médiathèque !</h2>
					<img src='/".$http->racine."/img/mediatheque.jpg' alt='Image mediatheque'>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius aliquam perferendis voluptas vel ea fugit qui doloribus maxime fugiat rem quo modi quas, sunt nostrum cupiditate placeat delectus, architecto. Cumque.</p>

			   </article>";
		return $index;
	}

	function afficherMenu()
	{
		$http = new \utils\HttpRequest();

		$menu="<nav>
				<ul class='inlineBlock'>
					<li><a href='/".$http->racine."/index.php/ArticleController/'>Accueil</a></li>
					<li><a href='/".$http->racine."/index.php/ArticleController/search'>Recherche</a></li>
					<li><a href='/".$http->racine."/index.php/ArticleController/search/all-article'>Catalogue</a></li>
			    </ul>
			   </nav>";
		return $menu;
	}

	public function afficherFoot()

	{
		$foot="<footer>
				<h5 class='center'>Copyright Team Beefy 2015</h5>
				<h6 class='center'>LP CISIIE</h6>
				</footer>";
		return $foot;
	}

	function afficherForm()
	{
		$http = new \utils\HttpRequest();
		$genre = \userApp\model\Article::listSelectGenre();
		$form = "<div>
					<h3 class='center'>Rechercher un document : </h3>
					<form action=\"/".$http->racine."/index.php/ArticleController/search\" method=\"POST\">
						<input type=\"text\" name=\"titre\" placeholder=\"Rechercher par titre... \" class=\"search\">
						<input type=\"text\" name=\"keyword\" placeholder=\"Mots clé...\" class=\"search\">
					<select name=\"type\" class=\"search\">
						<optgroup label='-- Sélectionner le support --'>
						<option value=\"null\">Peu importe</option> 
						<option value=\"livre\">Livre</option> 
						<option value=\"cd\">CD</option>
						<option value=\"dvd\">DVD</option>
					</select>";

					$form.="<form action=\"/".$http->racine."/index.php/ArticleController/searchGenre\" method=\"POST\">
							<select name='genre' class=\"search\">
							<optgroup label='-- Sélectionner le genre --'>
								<option value=\"null\">Peu importe</option> ";
					foreach ($genre as $val) {
						$form.="<option value='".utf8_encode(strtolower($val))."'>".utf8_encode($val)."</option>";
					}

					$form.="</select>

					<select name=\"dispo\" class=\"search\">
						<optgroup label='-- Disponibilité --'>
						<option value=\"null\">Peu importe</option> 
						<option value=\"-1\">Indisponible</option> 
						<option value=\"0\">Prêté</option>
						<option value=\"1\">Disponible</option>
						<option value=\"2\">Réservé</option>
					</select>
					<input type=\"submit\" name=\"rechercher\" value=\"Rechercher...\" class=\"search btn btnOrange\">
				</form>
			</div>";
			return $form;
	}

	function afficherCatalogue()
	{
		$http = new \utils\HttpRequest();
		$pdo = \utils\Connexion::getConnexion();

		$search = "";
		foreach ($this->research as $new) {
			$search.="<article class=\"jumboV2\">";
			$search.="<h4 class='titleHeight'>".utf8_encode($new->titre)."</h4>";
			$search.="<img src='/".$http->racine."/img/produits/".$new->imgLink."' alt='Image de la ressource ".utf8_encode($new->titre)."'>";
			$search.="<p class='underline'>Media : </p><p>".utf8_encode($new->type)."</p>";
			$search.="<p class='underline'>Catégorie : </p><p>".utf8_encode($new->genre)."</p>";
			$search.="<p class='underline'>Description : </p><p class='label'>".utf8_encode($new->label)."</p>";
			$search.="<p  class='underline'>Référence : </p><p>".$new->idArticle."</p>";
			switch ($new->isDispo) {
				case utf8_decode('Réservé'):
					$search.="<p class='alertInfo'>".utf8_encode($new->isDispo)."</p>";
					break;

				case 'Disponible':
					$search.="<p class='alertSucces'>".utf8_encode($new->isDispo)."</p>";
					break;
				
				case 'Indisponible':
					$search.="<p class='alertDanger'>".utf8_encode($new->isDispo)."</p>";
					break;

				case utf8_decode('En prêt'):
					$id = $new->idArticle;
					$requete2 = $pdo->prepare("SELECT dateEmprunt FROM emprunt WHERE idDoc=:id;");
				    $requete2->bindParam(':id', $id);
				    $resultObject2 = $requete2->execute();
				    if ($resultObject2) {
		    			while ($ligne2 = $requete2->fetch(\PDO::FETCH_OBJ)) {
		    				$search.="<p class='alertWarning'>".utf8_encode($new->isDispo)." - Retour le ".date("d-m-y", strtotime($ligne2->dateEmprunt.'+ 30days'))."</p>";
		    			}
		    		}
					break;


				case utf8_decode('Réservé'):
					$search.="<p class='alertInfo'>".utf8_encode($new->isDispo)."</p>";
					break;	
			}
			// $search.="<p>".utf8_encode($new->isDispo)."</p>";
			$search.="<a href='/".$http->racine."/index.php/ArticleController/searchID/".$new->idArticle."'>Voir plus...</a>";
			$search.="</article>";
		}
		return $search;
	}


}