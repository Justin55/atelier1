<?php

/***
*
*
* author : Henner Frédéric / Merlier Florian
*
*
***/

namespace staffApp\model;

class Adherent
{
	private $idUser;
	private $nom;
	private $prenom;
	private $tel;
	private $mail;
	private $dateNaissance;
	private $adresse;
	private $login;
	private $password;

	function __construct()
	{

	}

	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
	    //throw new \Exception($emess);
  	}

  	public function __set($attr_name, $attr_val) {
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	     // throw new \Exception($emess);
	   	}
  	}

################################################################################################################################
// Méthode qui ajoute dans la table Adherent
  	public function insertionAdherent()
  	{
	    $pdo = \utils\Connexion::getConnexion();

	    $requete_preparee = $pdo->prepare("INSERT INTO user ( nom, prenom, tel, mail, dateNaiss, adresse, login, password ) VALUES ( :nom, :prenom, :tel, :mail, :dateNaissance, :adresse, :login, :password )");
	  
	    $requete_preparee->bindParam(':nom', $this->nom);
	    $requete_preparee->bindParam(':prenom', $this->prenom);
	    $requete_preparee->bindParam(':tel', $this->tel);
	    $requete_preparee->bindParam(':mail', $this->mail);	    
	    $requete_preparee->bindParam(':dateNaissance', $this->dateNaissance);
	    $requete_preparee->bindParam(':adresse', $this->adresse);	    
	    $requete_preparee->bindParam(':login', $this->login);
	    $password = password_hash($this->password, PASSWORD_DEFAULT);
	    $requete_preparee->bindParam(':password', $password);

	    $requete_preparee->execute();
  	}
// Méthode qui update un adherent de la table User
  	public function updateAdherent()
  	{
  		$pdo = \utils\Connexion::getConnexion();

  		$query = $pdo->prepare("UPDATE user SET nom =:nom,
  												prenom =:prenom,
  												adresse =:adresse, 
  												tel =:tel, 
  												mail =:mail, 
  												dateNaiss =:dateNaissance
  								WHERE idUser = :idUser");
  		$query->bindParam(':nom', $this->nom);
	    $query->bindParam(':prenom', $this->prenom);
	    $query->bindParam(':adresse', $this->adresse);
	    $query->bindParam(':tel', $this->tel);
	    $query->bindParam(':mail', $this->mail);	    
	    $query->bindParam(':dateNaissance', $this->dateNaissance);

	    $query->execute();
	}
// Méthode qui retourne un tableau contenant la table user
  	public static function findAllAdherent()
  	{
  		$return = array();

		$pdo = \utils\Connexion::getConnexion();
	    
	    $query = $pdo->prepare("SELECT * FROM user");
	    $resu = $query->execute(); 	

	    if ($resu) {
	     	while ($ligne = $query->fetch(\PDO::FETCH_OBJ)) 
	     	{
	       		$a = new Adherent();
	       		$a->idUser = $ligne->idUser;
	       		$a->nom = $ligne->nom;
	       		$a->prenom = $ligne->prenom;
	       		$a->tel = $ligne->tel;
	       		$a->mail = $ligne->mail;
	       		$a->adresse = $ligne->adresse;
	       		$a->dateNaissance = $ligne->dateNaiss;
				$return[] = $a;
  			}
  			return $return;
  		}
  		else return -1;
	}
// Methode de suppression d'un adhérent par rapport à l'id
	public static function deleteAdherent($idUser)
	{
		$pdo = \utils\Connexion::getConnexion();

		$query = $pdo->prepare("DELETE FROM user where idUser=:idUser");
		$query->bindParam(':idUser',$idUser);
		$resu = $query->execute();
	} 


}