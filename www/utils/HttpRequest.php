<?php

namespace utils;


/**
 * Classe HttpRequest
 *    Analyseur d'URL qui permet de facilité l'extraction des paramètres 
 *    de la requête depuis le tableau $_SERVER
 *
 *   1) le contrôleur a utiliser ;
 *   2) l'action a réalisé ;
 *   3) les paramètres  nécessaires a l'action.
 * 
 */

class HttpRequest {

  private $control=null, $action=null, $param=null ;
  private $method=null, $get=null, $post=null;
  private $racine=null;
  
  /**
   *   Getter generique
   *
   *   fonction d'acces aux attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut accede
   *   et retourne sa valeur.
   *  
   *   @param String $attr_name attribute name 
   *   @return mixed
   */

  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
    throw new \Exception($emess);
  }
   
  
  /**
   *   Setter generique
   *
   *   fonction de modification des attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
   *  
   *   @param String $attr_name attribute name 
   *   @param mixed $attr_val attribute value
   */
  
  public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) 
      $this->$attr_name=$attr_val; 
    else{
      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
      throw new \Exception($emess);
    }
  }
  
  /**
   *   Constructeur: extrait toutes les informations  depuis le tableau 
   *                 $_SERVER et renseigne les attributs   
   *
   */
  public function __construct() {

    /* Les éléments de la requete sont dans  $_SERVER['PATH_INFO'] */
    /* avec le format format : /contrôleur/action/paramètre        */
    /* (Attention : certaines actions n'ont pas de paramètre)      */
    
    if( isset($_SERVER['PATH_INFO']) ) {
       $requete = $_SERVER['PATH_INFO'];

       /* Diviser la requete sur les slashs */
       $requete = trim( $requete , '/');
       $requete_coupee = explode('/', $requete) ;
       
       /* Renseigné les attributs */
       
       if (count( $requete_coupee) > 0)
	 $this->control = $requete_coupee[0];
       
       if (count( $requete_coupee) > 1)
	 $this->action  = $requete_coupee[1];
       
       if (count( $requete_coupee) > 2)
	 $this->param = $requete_coupee[2];
       
      }
    
    /* Renseigner la méthode d'accès (GET ou POST) */
    /* dans $_SERVER['REQUEST_METHOD']             */

    $this->method = $_SERVER['REQUEST_METHOD'];

    /* Renseigner les  paramètres de la méthode d'accès */
    /* les tableaux : $_GET et  $_POST */
    
    $this->get  = $_GET;
    $this->post = $_POST;

    /* Renseigner la racine du projet */
    /* le répertoire du script  */
    $this->racine = ltrim(dirname($_SERVER['SCRIPT_NAME']), '/') ;
    
    
  }
    
  

}