<?php

namespace utils;

class Connexion {
  
  private static $db_link ;
  private static $config_file='../conf/config.ini';
  

  private static function connect() { 
    try 
    {
      
      $config = parse_ini_file(self::$config_file );
      
      $dsn = "mysql:host=$config[hostname];dbname=$config[dbname]";
      $db = new \PDO($dsn, $config['user'], $config['password'],
          array(\PDO::ERRMODE_EXCEPTION => true,
                \PDO::ATTR_PERSISTENT => true));
    } 
    catch(\PDOException $e) 
    {
      echo "Erreur de connexion : $dsn (".$e->getMessage().")<br/>" ;
      exit;
    }
   
    $db->exec("SET CHARACTER SET utf8");
    self::$db_link = $db;
    
  }

  public static function getConnexion() {
    if (!isset(self::$db_link)) {
        self::connect();
    }
    return self::$db_link;
  }
}