<?php

function autoload( $classname ){
    $filename= '';
    $classname = ltrim( $classname , '\\');

    $filename= str_replace ('\\', DIRECTORY_SEPARATOR, $classname);
    $filename .= '.php';

    require_once $filename;
}

spl_autoload_register( 'autoload' );

