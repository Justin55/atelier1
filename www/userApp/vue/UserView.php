<?php 

namespace userApp\vue;

class UserView{

	private $user;

	public function __get($attr_name) 
	{
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
	    throw new \Exception($emess);
	}

	public function __set($attr_name, $attr_val) 
	{
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	      throw new \Exception($emess);
	    }
	}

	function afficher($action)
	{
		$html = "";

		$http = new \utils\HttpRequest();
		// $head = $this->afficherHead();
		// $menu = $this->afficherMenu();
		// $foot = $this->afficherFoot();

		// $html = "<!DOCTYPE html>
		// 		<html lang=\"fr\">
		// 		<head>
		// 			<meta http-equiv='content-type' content='text/html' charset=\"UTF-8\">
		// 			<title>user view</title>
		// 			<link rel='stylesheet' href='/".$http->racine."/style/screen.css' />
					
		// 		</head>
		// 		<body>";

		// $html.=$head;
		// $html.=$menu . "<div>";

		switch ($action) {
				case 'login':
				 	$html.=$this->afficherLogin();
				break;
				case 'connect':
				 	$html.=$this->afficherConnect();
				break;
				case 'connectPlus':
					$html.=$this->afficherConnectPlus();
					break;
		}
		// $html.="<div>";
		// $html.=$foot;
		// $html.="</body>
		// 		</html>";			

		return $html;
	}

	private function afficherLogin(){
		$log="<div>
				<h2>Bienvenue ".$this->user->login."</h2>
			  </div>";
		return $log;
	}

	public function afficherConnect()
	{
		$http = new \utils\HttpRequest();
		$head="<div>
				<form method=\"POST\" action='/".$http->racine."/indexHasan.php/UserController/head'>
					<input type=\"text\" name=\"login\" placeholder=\"login...\">
					<input type=\"password\" name=\"passw\" placeholder=\"password...\">
					<input type=\"submit\" name=\"connect\" value=\"Se connecter\">
				</form>	
				</div>";
		return $head;			
	}

	public function afficherConnectPlus()
	{
		$http = new \utils\HttpRequest();
		$head="<div>
				<form method=\"POST\" action='/".$http->racine."/indexHasan.php/UserController/head'>
					<input type=\"text\" name=\"login\" placeholder=\"login...\">
					<input type=\"password\" name=\"passw\" placeholder=\"password...\">
					<input type=\"submit\" name=\"connect\" value=\"Se connecter\">
				</form>	
				<p>Mauvais identifiant ou mot de passe</p>
				</div>";
		return $head;			
	}
}