<?php 

/***
*
* author : Merlier Florian et Henner Frédéric
*
***/

namespace staffApp\view;

class View 
{

	private $temp, $count;

function __construct()
	{

	}

	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
	    //throw new \Exception($emess);
  	}

  	public function __set($attr_name, $attr_val) {
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	      //throw new \Exception($emess);
	   	}
  	}

	public function afficher($action)
	{
		$http = new \utils\HttpRequest();
		$html = "<!DOCTYPE html>
						<html lang=\"fr\">
   							<head>
        						<meta  charset=\"UTF-8\">
        						<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        						<title>Médiatheque</title>
        						<link rel=\"icon\" type=\"image/png\" href=\"/".$http->racine."/img/favicon/favicon.png\" />
        						<link href=\"/".$http->racine."/style/screen.css\" media=\"screen\" rel=\"stylesheet\" type=\"text/css\" />
								<link href=\"/".$http->racine."/style/screen.css\" media=\"print\" rel=\"stylesheet\" type=\"text/css\" />
								<!--[if IE]>
								   	<link href=\"/".$http->racine."/style/screen.ie.css\" media=\"screen\" rel=\"stylesheet\" type=\"text/css\" />
								<![endif]-->        						
    						</head>

    						</head>
    						<body>";

    	$menu = $this->affiche_menu();
    	$html.= $menu;


    	$html.="<article class='sideNavVertical jumboV4''>";
		switch ($action)
		{
			case 'emprunt':
				$main = $this->affiche_emprunt();
				break;
			case 'retour':
				$main = $this->affiche_retour();
				break;
			case 'recapitulatif_all':
				$main = $this->affiche_recapitulatif();
				break;
			case 'recapitulatif_id';
				$main = $this->affiche_recapitulatif_id();
				break;
			case 'recapitulatif_retour' :
				$main = $this->affiche_recapitulatif_retour();
				break;
			case 'recapitulatif_emprunt' :
				$main = $this->affiche_recapitulatif_emprunt();
				break;
			case 'document':
				$main = $this->affiche_document();
				break;
			case 'adherent':
				$main = $this->affiche_adherent();
				break;
			case 'adherent_mod':
				$main = $this->affiche_modif_adherent();
				break;
			case 'adherent_updateTest':
				$main = $this->affiche_modif_adherent();
				break;
			case 'document_mod':
			 	$main = $this->affiche_document();
			 	break;
			case 'document_mod_script':
			 	$main = $this->affiche_modif_document();
			 	break;

		}

		$html.= $main;
			
		$html.="</article>";

    	
    	

    	$html.= "			</body>
						</html>";

		return $html;
	}

	// Méthode qui affiche le menu Staff
	public function affiche_menu()
	{
		$http = new \utils\HttpRequest();

		$menu =     			"<nav class='navVertical'>

    								<h1>Staff</h1>
    								<ul class='block'>
    									<li><a href=\"/".$http->racine."/index2.php/StaffController/emprunt\">Gérer Emprunt</a></li>
    									<li><a href=\"/".$http->racine."/index2.php/StaffController/retour\">Gérer Retour</a></li>
    									<li><a href=\"/".$http->racine."/index2.php/StaffController/document\">Gérer Documents</a></li>
    									<li><a href=\"/".$http->racine."/index2.php/StaffController/adherent\">Gérer Adhérents</a></li>
    								</ul>
    							</nav>";
    	return $menu;
	}
// Méthode qui affiche la page Emprunt
	public function affiche_emprunt()
	{
		$http = new \utils\HttpRequest();

		$formemprunt = "    	<div>
    								<h2>Gérer les emprunts :</h2>
    								<form method=\"post\" action=\"/".$http->racine."/index2.php/StaffController/emprunt_add\">
										<input type=\"text\" name=\"iduser\" placeholder=\"ID Adhérent\">
										<input type=\"text\" name=\"iddoc\" placeholder=\"Référence Doc\">
										<input type=\"submit\" value=\"Ajouter l'emprunt\">
									</form>
									<a href=\"/".$http->racine."/index2.php/StaffController/empruntRecapitulatif\">Finir l'emprunt des documents</a>
									<a href=\"/".$http->racine."/index2.php/StaffController/recapitulatif_all\">Récapitulatif de tout les emprunts</a>
									<form method=\"post\" action=\"/".$http->racine."/index2.php/StaffController/recapitulatif_all\">
										<input type=\"text\" name=\"iduser2\" placeholder=\"ID de l'Adhérent\">
										<input type=\"submit\" value=\"Afficher le Récapitulatif par rapport à l'id\">
									</form>
								</div>";
		return $formemprunt;
	}	
// Méthode qui affiche la page Retour
	public function affiche_retour()
	{
		$http = new \utils\HttpRequest();
		$formretour = "    		<div>
    								<h2>Gérer les retours :</h2>
    								<form method=\"post\" action=\"/".$http->racine."/index2.php/StaffController/retour_add\">
										<input type=\"text\" name=\"iddoc\" placeholder=\"Référence Doc\">
										<input type=\"submit\" value='Retour du Document'>
									</form>
									<a href=\"/".$http->racine."/index2.php/StaffController/retourRecapitulatif\">Finir le retour des documents</a>
									<form method=\"post\" action=\"/".$http->racine."/index2.php/StaffController/recapitulatif_retour\">
										<input type=\"text\" name=\"iduser2\" placeholder=\"ID de l'Adhérent\">
										<input type=\"submit\" value=\"Afficher le Récapitulatif par rapport à l'id\">
									</form>
								</div>";
		return $formretour;
	}	
// Méthode qui affiche la page avec le recapitulatif de tout les emprunts encore en cours
	public function affiche_recapitulatif()
	{
		$e = new \staffApp\model\Emprunt();


		$recap = "				<div>
									<h2>Récapitulatif : </h2>";
		$count= 0;
		$e = \staffApp\model\Emprunt::findAllEmprunt();

		foreach ($e as $new) {
		$count += 1 ;
		$recap .= "<div><p><span>Id Emprunt : </span><span>".$new->idEmprunt."</span></p>";
		$recap .= "<p><span>Nom : </span><span>".(\userApp\model\User::findByID($new->idUser)->nom)." ".(\userApp\model\User::findByID($new->idUser)->prenom)."</span></p>";
		$recap .= "<p><span>Id Adherent : </span><span>".$new->idUser."</span></p>";
		$recap .= "<p><span>Id du document : </span><span>".$new->idDoc."</span></p>";
		$recap .= "<p><span>Titre : </span><span>".(utf8_encode(\userApp\model\Article::findByID($new->idDoc)->titre))."</span></p>";
		$recap .= "<p><span>Date d'emprunt : </span><span>".date("d-m-y", strtotime($new->dateEmprunt))."</span></p>";
		$recap .= "<p><span>Date de retour : </span><span>".date("d-m-y", strtotime($new->dateEmprunt.'+ 30days'))."</span></p></div>";
		}
		$recap .= "<p class='title center'> Il y a ".$count." document(s) emprunté(s).</p>";

		$recap .=				"</div>";
		return $recap;
	}
// Méthode qui affiche un récapitulatif des emprunts encore en cours par rapport a l'id d'un adhérent
	public function affiche_recapitulatif_id()
	{
		$e = new \staffApp\model\Emprunt();


		$recap = "				<div>
									<h2>Récapitulatif</h2>";
		$recap .=								$this->affichageListRecapPerUser($this->temp);

		$recap .=				"</div>";
		return $recap;
	}
// Méthode qui affiche le récapitulatif apres avoir fini les retours
	public function affiche_recapitulatif_retour()
	{
		$count = 0;
		$recap = "				<div>
									<h2>Récapitulatif des retours</h2>";
									
		if (isset($_SESSION['tableauRetour'])) {
			$recap .= "<h3> Le(s) titre(s) des document(s) sont : </h3>";
			foreach ($_SESSION['tableauRetour'] as $value) {
				$a = \userApp\model\Article::findById(intval($value));
				$recap .= "<p>". utf8_encode($a->titre) . "</p>";
				$count += 1;
			}
			if (isset($_SESSION['IDr'])) {
				foreach ($_SESSION['IDr'] as $value) {
					$id = $value->idUser;
				}
				$u = \userApp\model\User::findById($id);
				$recap .= "<p>".$u->nom." ".$u->prenom." a rendu(s) ".$count." document(s) </p>";
				$recap .=		"</div>";
				session_destroy();
			}
		}
		else
			$recap .= "<p> Il n'y a pas de document rendu pour le moment. </p>";

			if (isset($id)) {
				$recap .= $this->affichageListRecapPerUser($id);
			}
		return $recap;
	}
// Méthode qui affiche le récapitulatif apres avoir fini les emprunts
	public function affiche_recapitulatif_emprunt()
	{
		$count = 0;
		$recap = "				<div>
									<h2>Récapitulatif des emprunts</h2>";
									

		if (isset($_SESSION['tableauEmprunt'])) {
			$recap .= "<h3> Le(s) titre(s) des document(s) sont : </h3>";
			foreach ($_SESSION['tableauEmprunt'] as $value) {
				$a = \userApp\model\Article::findById(intval($value));
				$recap .= "<p>". utf8_encode($a->titre) . "</p>";
				$count += 1;
			}
			if (isset($_SESSION['ID'])) {
				$u = \userApp\model\User::findById($_SESSION['ID']);
				$recap .= "<p>".$u->nom." ".$u->prenom." a emprunté(s) ".$count." document(s) </p>";
				$recap .=		"</div>";
				session_destroy();
			}
		}
		else
			$recap .= "<p> Il n'y a pas d'emprunt pour le moment. </p>";

		return $recap;		
	}
// Méthode qui affiche la page gestion des documents
	public function affiche_document()
	{
		$http = new \utils\HttpRequest();
		$gererdoc = "			<div>
									<h2>Gérer les documents : </h2>
									<div>
										<h3>Ajouter un document : </h3>
										<form method=\"post\" enctype=\"multipart/form-data\" action=\"/".$http->racine."/index2.php/StaffController/document_add\">
											<input type=\"text\" name=\"nom\" placeholder=\"Nom\"></input>										
											<select name=\"type\" >
												<optgroup label='-- Sélectionner le support --'>
												<option value=\"Livre\">Livre</option> 
												<option value=\"CD\">CD</option>
												<option value=\"DVD\">DVD</option>
											</select>
											<input type=\"text\" name=\"genre\" placeholder=\"Genre\"></input>
											<textarea name=\"description\" placeholder=\"Description du document\"></textarea>
											<input type=\"file\" name=\"icone\" id=\"icone\" class='file'/>
											<input type=\"submit\" value='Ajouter'>
										</form>
									</div>
									<div>
									<h3>Liste documents</h3>
										";
										$gererdoc .= $this->AfficheAllDocument();
										$gererdoc .="
										

									</div>
								</div>";
		return $gererdoc;
	}

// Méthode qui affiche la page gestion des adhérents

	public function affiche_adherent()
	{

		$http = new \utils\HttpRequest();
		$gereradh = "			<div>
									<h2>Gérer les adhérents : </h2>
									<div>
										<h3>Ajouter un adhérent : </h3>
										<form method=\"post\" action=\"/".$http->racine."/index2.php/StaffController/adherent_add\">
											<input type=\"text\" name=\"nom\" placeholder=\"Nom\">
											<input type=\"text\" name=\"prenom\" placeholder=\"Prénom\">
											<input type=\"text\" name=\"adresse\" placeholder=\"Adresse\">
											<input type=\"text\" name=\"age\" placeholder=\"Date de naissance\">
											<input type=\"text\" name=\"mail\" placeholder=\"Email\">
											<input type=\"text\" name=\"telephone\" placeholder=\"Téléphone\">
											<input type=\"text\" name=\"login\" placeholder=\"Identifiant\">
											<input type=\"text\" name=\"password\" placeholder=\"Mot de Passe\">
											<input type=\"submit\" value='Ajouter'>
										</form>
									</div>
									<div>
										<h3>Liste adhérents : </h3>";
								$gereradh.=$this->AfficheAllAdherent();
									$gereradh.="</div>
								</div>";
		return $gereradh;
	}
######  ######
	public function affiche_modif_adherent()
	{
		$http = new \utils\HttpRequest();
		//$all = \staffApp\model\Adherent::findAllAdherent();
		$adhe = \userApp\model\User::findbyId($this->temp);
		$gereradh = "			<div>
									<h2>Gérer adhérents</h2>
									<h3>Modifier un adhérent</h3>
									<form method=\"post\" action=\"/".$http->racine."/index2.php/StaffController/adherent_updateTest\">
									<input type=\"hidden\" name=\"id\" placeholder=\"Nom\" value=\"".$adhe->idUser."\">
		 								<input type=\"text\" name=\"nom\" placeholder=\"Nom\" value=\"".$adhe->nom."\">
										<input type=\"text\" name=\"prenom\" placeholder=\"Prénom\" value=\"".$adhe->prenom."\">
										<input type=\"text\" name=\"adresse\" placeholder=\"Adresse\" value=\"".$adhe->adresse."\">
										<input type=\"text\" name=\"age\" placeholder=\"Date de naissance (yyyy-mm-dd)\" value=\"".$adhe->dateNaiss."\">
										<input type=\"text\" name=\"mail\" placeholder=\"Email\" value=\"".$adhe->mail."\">
										<input type=\"text\" name=\"telephone\" placeholder=\"Téléphone\" value=\"".$adhe->tel."\">
										<input type=\"submit\" name=\"name_action\" value=\"Modification\">
									</form>								
								</div>";
				
		return $gereradh;
	}

	public static function AfficheAllAdherent()
	{
		$http = new \utils\HttpRequest();
		$gereradh = ""; $count= 0;
		$test = \staffApp\model\Adherent::findAllAdherent();
		
		foreach ($test as $new) {
		$count += 1 ;
		$gereradh .= "<form method=\"post\" action=\"/".$http->racine."/index2.php/StaffController/adherent_mod\">";
		$gereradh .= "<input type=\"hidden\" name=\"idUser\" value=\"".$new->idUser."\">";
		$gereradh .= "<p> Id Adhérent: ".$new->idUser."</p>";
		$gereradh .= "<p> Nom : ".$new->nom." ".$new->prenom."</p>";
		$gereradh .= "<p> Téléphone : ".$new->tel."</p>";
		$gereradh .= "<p> Mail: ".$new->mail."</p>";
		$gereradh .= "<p> Date de naissance: ".$new->dateNaissance."</p>";
		$gereradh .= "<input type=\"submit\" name=\"name_action\" value=\"Modifier\">";
		$gereradh .= "<input type=\"submit\" name=\"name_action\" value=\"Supprimer\">";
		$gereradh .= "</form>";
		$gereradh .= "<hr>";
		}
		$gereradh .= "<p> Il y a ".$count." adhérent(s) répertorié(s)</p>";
		return $gereradh;
	}
##### Ajout Document	

	public static function AfficheAllDocument()
	{
		$http = new \utils\HttpRequest();
		$gererdoc = ""; $count= 0;
		$test = \userApp\model\Article::findAll();
		
		foreach ($test as $new) {
		$count += 1 ;
		$gererdoc.= "<form method=\"post\" action=\"/".$http->racine."/index2.php/StaffController/document_mod\">";
		$gererdoc.= "<input type=\"hidden\" name=\"idDoc\" value=\"".$new->idArticle."\">";
		$gererdoc.= "<p> Id Document: ".$new->idArticle."</p>";
		$gererdoc.= "<p> Titre du document : ".utf8_encode($new->titre)."</p>";
		$gererdoc.= "<p> Type du document : ".utf8_encode($new->type)."</p>";
		$gererdoc.= "<p> Genre du document : ".utf8_encode($new->genre)."</p>";
		$gererdoc.= "<p> Description du document : ".utf8_encode($new->label)."</p>";
		$gererdoc.= "<input type=\"submit\" name=\"name_action\" value=\"Modifier\">";
		$gererdoc.= "<input type=\"submit\" name=\"name_action\" value=\"Supprimer\">";
		$gererdoc.= "</form>";
		$gererdoc.= "<hr>";
		}
		$gererdoc.= "<p> Il y a ".$count." document(s) enregistré(s)</p>";
		return $gererdoc;
	}

// Méthode qui affiche un récapitulatif par rapport à un ID d'adherent
	public static function affichageListRecapPerUser($user)
	{
		$recap2 = ""; $count= 0; $nom= ""; $prenom="";
		$test = \staffApp\model\Emprunt::findEmpruntByUser($user);

		foreach ($test as $new) {
		$count += 1 ;
		$nom = (\userApp\model\User::findByID($new->idUser)->nom);
		$prenom = (\userApp\model\User::findByID($new->idUser)->prenom);

		$titre = (\userApp\model\Article::findByID($new->idDoc)->titre);
		$recap2 .= "<p> Id Emprunt: ".$new->idEmprunt."</p>";
		$recap2 .= "<p> Id Adherent: ".$new->idUser."</p>";
		$recap2 .= "<p> Nom de l'adherent: ".$nom."</p>";
		$recap2 .= "<p> Prénom de l'adherent: ".$prenom."</p>";
		$recap2 .= "<p> Id du document: ".$new->idDoc."</p>";
		$recap2 .= "<p> Titre du document: ".utf8_encode($titre)."</p>";
		$recap2 .= "<p> Date d'emprunt: ".date("d-m-y", strtotime($new->dateEmprunt))."</p>";
		$recap2 .= "<p> Date de retour: ".date("d-m-y", strtotime($new->dateEmprunt.'+ 30days'))."</p>";
		$recap2 .= "<hr>";
		}
		if($count==0)
		{
			$recap2 .= "<p> Aucun document emprunté.";
		}
		else $recap2 .= "<p> Il y a ".$count." document(s) emprunté(s) par ".$nom." ".$prenom;

		return $recap2;
	}
// Méthode qui affiche un récapitulatif des emprunts
	public static function affichageListRecap()
	{
		$recap2 = ""; $count= 0;
		$test = \staffApp\model\Emprunt::findAllEmprunt();
		

		foreach ($test as $new) {
		$count += 1 ;
		$recap2 .= "<p> Id Emprunt: ".$new->idEmprunt."</p>";
		$recap2 .= "<p> Nom : ".(\userApp\model\User::findByID($new->idUser)->nom)." ".(\userApp\model\User::findByID($new->idUser)->prenom)."</p>";
		$recap2 .= "<p> Id Adherent: ".$new->idUser."</p>";
		$recap2 .= "<p> Id du document: ".$new->idDoc."</p>";
		$recap2 .= "<p> Titre: ".(utf8_encode(\userApp\model\Article::findByID($new->idDoc)->titre))."</p>";
		$recap2 .= "<p> Date d'emprunt: ".date("d-m-y", strtotime($new->dateEmprunt))."</p>";
		$recap2 .= "<p> Date de retour: ".date("d-m-y", strtotime($new->dateEmprunt.'+ 30days'))."</p>";
		$recap2 .= "<hr>";

		}

		if($count==0)
		{
			$recap2 .= "<p> Aucun document emprunté.";
		}
		else $recap2 .= "<p> Il y a ".$count." document(s) emprunté(s) par ".$nom." ".$prenom;
		return $recap2;
	}
	public function affiche_modif_document()
	{
		$http = new \utils\HttpRequest();
		$doc = \userApp\model\Article::findbyId($this->temp);
		$gererdoc = "			<div>
									<h2>Gérer Document</h2>
									<h3>Modifier un document</h3>
									<form method=\"post\" action=\"/".$http->racine."/index2.php/StaffController/document_mod_script\">
									<input type=\"hidden\" name=\"idDoc\" placeholder=\"Nom\" value=\"".$doc->idArticle."\">
										<input type=\"text\" name=\"titre\" placeholder=\"titre\" value=\"".utf8_encode($doc->titre)."\">
										<input type=\"text\" name=\"type\" placeholder=\"type\" value=\"".utf8_encode($doc->type)."\">
										<input type=\"text\" name=\"genre\" placeholder=\"genre\" value=\"".utf8_encode($doc->genre)."\">
										<textarea class='search' name=\"label\">".utf8_encode($doc->label)."</textarea>
										<input type=\"submit\" name=\"name_action\" value=\"Modification\">
									</form>		
								</div>";
				
		return $gererdoc;
	}

						


	

}