<?php

/***
*
* author : Henner Frédéric
*
***/

namespace staffApp\model;

class Retour
{

	private $idDoc;

	function __construct()
	{

	}

	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
	    throw new \Exception($emess);
  	}

  	public function __set($attr_name, $attr_val) {
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	      throw new \Exception($emess);
	   	}
  	}

##########################################################################################################################################
// Méthode qui supprime dans la table emprunt
	public function insertionRetour() {
		$pdo = \utils\Connexion::getConnexion();

		$requete_preparee = $pdo->prepare("DELETE FROM emprunt WHERE idDoc=:id");
		$requete_preparee->bindParam(':id',$this->idDoc);
		$requete_preparee->execute();

	    $requete_preparee2 = $pdo->prepare("UPDATE document SET isDispo=1 WHERE id=:iddoc");
		
		$requete_preparee2->bindParam(':iddoc', $this->idDoc);

		$requete_preparee2->execute();
	}
// Méthode qui retourne les id des documents dans la table emprunt
	public function findAllDocEmprunt()
	{
		$tab = array();
		$pdo = \utils\Connexion::getConnexion();

		$requete_preparee = $pdo->prepare("SELECT idDoc FROM emprunt");
		$requete_preparee->execute();

			while ($ligne = $requete_preparee->fetch(\PDO::FETCH_OBJ) )  {
					$tab[]=$ligne;
			}

			return $tab;
	}
// Méthode qui retourne les valeurs des id et de la disponibilités de la table document
	public function findAllDocDocument()
	{
		$tab = array();
		$pdo = \utils\Connexion::getConnexion();

		$requete_preparee = $pdo->prepare("SELECT id,isDispo FROM document");
		$requete_preparee->execute();

			while ($ligne = $requete_preparee->fetch(\PDO::FETCH_OBJ) )  {
					$tab[]=$ligne;
			}

			return $tab;
	}
// Méthode qui retourne les id des utilisateurs de la table emprunt par rapport à l'id d'un document
	public function findIDbyIDdoc($idDOC)
	{
		$tab = array();
		$pdo = \utils\Connexion::getConnexion();

		$requete_preparee = $pdo->prepare("SELECT idUser FROM emprunt WHERE idDoc=:iddoc");
		$requete_preparee->bindParam(':iddoc', $idDOC);
		$requete_preparee->execute();
			while ($ligne = $requete_preparee->fetch(\PDO::FETCH_OBJ) )  {
				$tab[]=$ligne;
			}
		return $tab;
	}

						   	# # # # End Fred # # # #
}