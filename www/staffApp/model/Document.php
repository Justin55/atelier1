<?php

/***
*
*
* author : Henner Frédéric / Merlier Florian
*
*
***/

namespace staffApp\model;

class Document
{

	private $idDoc;
	private $titre;
	private $type;
	private $genre;
	private $nom;
	private $description;

	function __construct()
	{

	}

	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
	    throw new \Exception($emess);
  	}

  	public function __set($attr_name, $attr_val) {
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	      throw new \Exception($emess);
	   	}
  	}

  	########################### Florian ################################

  	public static function deleteDocument($id)
	{
		$pdo = \utils\Connexion::getConnexion();

		$query = $pdo->prepare("DELETE FROM document where id=:id");
		$query->bindParam(':id',$id);
		$resu = $query->execute();
	} 

	public function updateDocument()
  	{
  		$pdo = \utils\Connexion::getConnexion();

  		$query = $pdo->prepare("UPDATE document SET id =:id,
  												titre =:titre,
  												type =:type, 
  												genre =:genre, 
  												label =:label			
  								WHERE idDoc = :idDoc");
  		$query->bindParam(':id', $this->id);
	    $query->bindParam(':titre', $this->titre);
	    $query->bindParam(':type', $this->type);
	    $query->bindParam(':genre', $this->genre);
	    $query->bindParam(':label', $this->label);	    

	    $query->execute();
	}
}