<?php 

/***
*
* author : Merlier Florian et Henner Frédéric
*
***/

namespace staffApp\model;

class Emprunt
{
	private $idEmprunt;
	private $idUser;
	private $idDoc;
	private $dateEmprunt;
	private $disp;

	function __construct()
	{

	}

	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
	    throw new \Exception($emess);
  	}

  	public function __set($attr_name, $attr_val) {
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	      throw new \Exception($emess);
	   	}
  	}

################################################################################################################################
// Méthode qui ajoute dans la table emprunt
	public function insertionEmprunt() {

	    $dateEmprunt = date("y-m-d");

	    $pdo = \utils\Connexion::getConnexion();

	    $requete_preparee = $pdo->prepare("INSERT INTO emprunt ( idUser, idDoc, dateEmprunt ) VALUES ( :User, :Doc, :Date );");
	  
	    $requete_preparee->bindParam(':User', $this->idUser);
	    $requete_preparee->bindParam(':Doc', $this->idDoc);
	    $requete_preparee->bindParam(':Date', $this->dateEmprunt);

	    $requete_preparee->execute();

	    $requete_preparee2 = $pdo->prepare("UPDATE document SET isDispo=0 WHERE id=:iddoc");
		
		$requete_preparee2->bindParam(':iddoc', $this->idDoc);

		$requete_preparee2->execute();
	}
// Méthode qui retourne un table des id des adhérents de la table user
	public function findAllIDUser()
	{
		$tab = array();
		$pdo = \utils\Connexion::getConnexion();

		$requete_preparee = $pdo->prepare("SELECT idUser FROM user");
		$requete_preparee->execute();

			while ($ligne = $requete_preparee->fetch(\PDO::FETCH_OBJ) )  {
					$tab[]=$ligne;
			}

		return $tab;

	}
// Méthode qui retourne les valeurs des id et de la disponibilités de la table document
	public function findAllDocDocument()
	{
		$tab = array();
		$pdo = \utils\Connexion::getConnexion();

		$requete_preparee = $pdo->prepare("SELECT id,isDispo FROM document");
		$requete_preparee->execute();

			while ($ligne = $requete_preparee->fetch(\PDO::FETCH_OBJ) )  {
					$tab[]=$ligne;
			}

			return $tab;
	}
// Méthode qui retourne un table de la base emprunt par rapport a un ID
	public static function findEmpruntByUser($idUser)
	{
		$return = array();

		$pdo = \utils\Connexion::getConnexion();
	    
	    $query = $pdo->prepare("SELECT * FROM emprunt WHERE idUser=:idUser");
	    $query->bindParam(':idUser',$idUser);
	    $resu = $query->execute();
	    if ($resu) {
	     	while ($ligne = $query->fetch(\PDO::FETCH_OBJ)) {
	       		$new = new \staffApp\model\Emprunt();
	       		$new->idEmprunt = $ligne->idEmprunt;
	       		$new->idUser = $ligne->idUser;
	       		$new->idDoc = $ligne->idDoc;
	       		$new->dateEmprunt = $ligne->dateEmprunt;
	       		$return[] = $new;
	   		}
	   		return $return;
		}
		else return -1;
	}
// Retourne un tableau avec toute les valeurs de la table emprunt
	public static function findAllEmprunt()
	{
		$count = 0;
		$return = array();

		$pdo = \utils\Connexion::getConnexion();
	    
	    $query = $pdo->prepare("SELECT * FROM emprunt");
	    $resu = $query->execute(); 			
	    
	    if ($resu) {
	     	while ($ligne = $query->fetch(\PDO::FETCH_OBJ)) {
	       		$new = new Emprunt();
	       		$new->idEmprunt = $ligne->idEmprunt;
	       		$new->idUser = $ligne->idUser;
	       		$new->idDoc = $ligne->idDoc;
	       		$new->dateEmprunt = $ligne->dateEmprunt;
				$return[] = $new;
	   		}
	   		return $return;
		}
		else return -1;
	}
}
