<?php  

session_start();

require_once("conf/autoload.php");
require_once("utils/password.php");

$http = new \utils\HttpRequest();

$controller = new \userApp\control\ArticleController($http);
$controller->dispatch();

if ($controller->http->action==null) {
	$view = new \userApp\vue\View();
	echo $view->afficher('index');

}


