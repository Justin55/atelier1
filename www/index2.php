<?php

session_start();


require_once("conf/autoload.php");
require_once("utils/password.php");

$http = new \utils\HttpRequest();

$control = new \staffApp\controller\StaffController($http);
$control->dispatch();


if ($control->http->action==null) {
	$view = new \staffApp\view\View();
	echo $view->afficher('emprunt');

}